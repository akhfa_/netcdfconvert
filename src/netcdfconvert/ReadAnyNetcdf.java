/*
 * Note: These examples use the old version 1 Java netCDF interface,
 * which we do not recommend for new development. Instead, please use
 * NetCDF Java Library (Version 2), which is more efficient, simpler,
 * and provides better support for remote access using HTTP or
 * DODS. Similar examples are available in the NetCDF Java (version 2)
 * User's Manual.
 */

package netcdfconvert;

import ucar.netcdf.Netcdf;
import ucar.netcdf.NetcdfFile;
import ucar.netcdf.Variable;
import ucar.netcdf.VariableIterator;

/** 
 * Read all the data from an existing netCDF file of unknown structure.
 * This only outputs variable names as they are being read, without
 * doing anything with the data, so it may useful for timing and tuning.
 *
 * @author: Russ Rew
 * @version: $Id: ReadAnyNetcdf.java,v 1.9 1998/07/17 15:24:32 russ Exp $ */
public class ReadAnyNetcdf {

    static String fileName;

    /** 
     * Reads an existing netCDF file with a specified file name.
     *
     * @param args name of netCDF file to be read.
     */
    public static void main(String[] args) {
	
	if (args.length == 1)
	    fileName = args[0];
	else {
	    System.err.println("no netCDF file name specified, exiting ...");
	    System.exit(-1);
	}

	try {
	    Netcdf nc = new NetcdfFile(fileName, true); // open it readonly
	    VariableIterator vi = nc.iterator();
	    while(vi.hasNext()) {
		Variable var = vi.next();
		System.out.println(var.getName() + " ...");
		// just throw away the data, read it in for timing and tuning
		var.copyout(new int[var.getRank()], 
			    var.getLengths());
	    }
	} catch (java.io.IOException e) {
	    e.printStackTrace();
	}

    }
}
