/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package netcdfconvert;

import com.opencsv.CSVReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import ucar.netcdf.Attribute;
import ucar.netcdf.Dimension;
import ucar.netcdf.NetcdfFile;
import ucar.netcdf.ProtoVariable;
import ucar.netcdf.Schema;
import ucar.netcdf.UnlimitedDimension;
import ucar.netcdf.Variable;

/**
 *
 * @author akhfa
 */
public class createChemi {
    static String judul = "wrfchemioke_d01";
    static String [] nextLine;
    public static void main(String [] args) throws FileNotFoundException, IOException
    {
        /* create netcdf */
        Dimension timeD = new UnlimitedDimension("time");
	Dimension westEastD = new Dimension("west_east", 76);
	Dimension southNorthD = new Dimension("south_north", 76);
        Dimension dateStrLenth = new Dimension(("DateStrLen"), 19);
        Dimension emissionD = new Dimension("emissions_zdim", 1);
        
        ProtoVariable Time = new ProtoVariable("T", char.class, new Dimension[]{timeD, dateStrLenth});
        
        String PM10Name = "PM10";
        Class PM10Type = float.class;

        Dimension [] PM10Dims = {timeD, emissionD, southNorthD, westEastD};
        
        Attribute PM10Unit = new Attribute("units", "ug/m^3");
        Attribute[] PM10Atts = {PM10Unit};
        
        ProtoVariable PM10Proto = new ProtoVariable(PM10Name, PM10Type, PM10Dims, PM10Atts);
        
        Attribute globalTitle = new Attribute("TITLE", "Global Title");
        
        Schema schema = new Schema(new ProtoVariable[] {PM10Proto}, new Attribute[]{globalTitle});
        
        /* writing data nc */
        NetcdfFile nc = new NetcdfFile(judul, true, true, schema);
        
            /* Baca csv dan skip baris 1 dan 2 */
            CSVReader reader = new CSVReader(new FileReader("file.csv"));
            nextLine = reader.readNext();
            nextLine = reader.readNext();
            
        Variable VarPm10 = nc.get("PM10");
        for (int timeIterator = 0; timeIterator < 1; timeIterator++)
        {
            for(int emisIterator = 0; emisIterator < emissionD.getLength(); emisIterator++)
            {
                for (int southNorthIterator = 0; southNorthIterator < southNorthD.getLength(); southNorthIterator++)
                {
                    for(int westEastIterator = 0; westEastIterator < westEastD.getLength(); westEastIterator++)
                    {
                        nextLine = reader.readNext();
                        //System.out.println(nextLine[3]);
                        VarPm10.setFloat(new int[]{timeIterator, emisIterator, southNorthIterator, westEastIterator}, Float.parseFloat(nextLine[3]));
                    }
                }
            }
        }
        nc.close();
        
        /* read csv */
        //CSVReader reader = new CSVReader(new FileReader("file.csv"));
        //for(int i = 0; i < 10; i++)
        //{
        //    nextLine = reader.readNext();
        //    System.out.println(nextLine[0] + "\t" + nextLine[1] + "\t" + nextLine[2] + "\t" + nextLine[3]);
        //}
    }
}
