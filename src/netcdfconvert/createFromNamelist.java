/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package netcdfconvert;

import com.opencsv.CSVReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ucar.netcdf.Attribute;
import ucar.netcdf.Dimension;
import ucar.netcdf.NetcdfFile;
import ucar.netcdf.ProtoVariable;
import ucar.netcdf.Schema;
import ucar.netcdf.UnlimitedDimension;
import ucar.netcdf.Variable;

/**
 *
 * @author akhfa
 */
public class createFromNamelist {
    public static void main(String [] args)
    {
        int westEast = 0;
        int southNorth = 0;
        String startDate = "";
        
        //Mengambil x,y,startdateStringBuilder from namelist.input
        try {
            BufferedReader br = new BufferedReader(new FileReader("namelist.input"));
            StringBuilder sb = new StringBuilder();
            StringBuilder startdateStringBuilder = new StringBuilder();
            String line = br.readLine();
            System.out.println(line);
            while(!(line = br.readLine()).startsWith("&physics"))
            {
                if(line.startsWith("start"))
                {
                    //ambil tahun
                    startdateStringBuilder.append(line.substring(line.length() - 5, line.length()-1));
                    startdateStringBuilder.append("-");
                    
                    //ambil bulan
                    line = br.readLine();
                    startdateStringBuilder.append(line.substring(line.length() - 3, line.length()-1));
                    startdateStringBuilder.append("-");
                    
                    //ambil hari
                    line = br.readLine();
                    startdateStringBuilder.append(line.substring(line.length() - 3, line.length()-1));
                    startdateStringBuilder.append("_");
                    
                    //ambil jam
                    line = br.readLine();
                    startdateStringBuilder.append(line.substring(line.length() - 3, line.length()-1));
                    startdateStringBuilder.append(":");
                    
                    //ambil menit
                    line = br.readLine();
                    startdateStringBuilder.append(line.substring(line.length() - 3, line.length()-1));
                    startdateStringBuilder.append(":");
                    
                    //ambil detik
                    line = br.readLine();
                    startdateStringBuilder.append(line.substring(line.length() - 3, line.length()-1));
                    
                    //Build startdateStringBuilder
                    startDate = startdateStringBuilder.toString();
                }
                
                //ambil westEast dimension, asumsi dimensi >= 10
                if(line.startsWith("e_we"))
                {
                    westEast = Integer.parseInt(line.substring(line.length() - 3, line.length()-1));
                }
                
                //ambil southNorth dimension, asumsi dimensi >= 10
                if(line.startsWith("e_sn"))
                {
                    southNorth = Integer.parseInt(line.substring(line.length() - 3, line.length()-1));
                }
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(createFromNamelist.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(createFromNamelist.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //Mengolah file csv
        final String NamaFileCsv = "filetaFINALtanpaIndex.csv";
        CSVReader reader;
        String NamaFileNc = "wrfchemi_d03";
        
        String [] Line;
        List<Attribute> lisOfGlobalAttribute = new ArrayList<>();
        List<ProtoVariable> listOfProtoVariable = new ArrayList<>();
            List<String> listOfNamaProto = new ArrayList<>();
        
        List<Variable> listOfVariable = new ArrayList<>();
        Variable [] arrayOfVariable;
        
        Dimension timeD;
        Dimension westEastD;
        Dimension southNorthD;
        Dimension dateStrLengthD;
        Dimension emissionD;
        
        Schema schema = null;
        
        
        try {
            reader = new CSVReader(new FileReader(NamaFileCsv));
            
            /* Bikin dimensi. Sementara statis dulu */
            timeD = new UnlimitedDimension("Time");
            westEastD = new Dimension("west_east", westEast);
            southNorthD = new Dimension("south_north", southNorth);
            dateStrLengthD = new Dimension(("DateStrLen"), 19);
            emissionD = new Dimension("emissions_zdim", 1);
            
            /* Baca global attribut */
            Line = reader.readNext();   //Ambil Attribute. Ini cuma baris dummy
            while(!(Line = reader.readNext())[0].equals("end"))
            {
                try
                {
                    //if(Line[1].endsWith("f"))
                    //{
                        lisOfGlobalAttribute.add(new Attribute(Line[0], (float)Float.parseFloat(Line[1])));
                    //}

                    //else
                    //{
                        //lisOfGlobalAttribute.add(new Attribute(Line[0], Integer.parseInt(Line[1])));
                    //}
                }
                catch(NumberFormatException e)
                {
                    lisOfGlobalAttribute.add(new Attribute(Line[0], Line[1]));
                }
                
            }   System.out.println("Keluar dari while attribut");
            
            /* Mulai ambil jenis chem. Dimulai dari baris yang isinya "data" */
            Line = reader.readNext(); //Ambil data. Ini cuma baris dummy
            Line = reader.readNext(); //Mengambil nextLine (daftar nama variabel dalam 1 baris csv)
            
            //Add protovariable Times
            listOfProtoVariable.add(new ProtoVariable("Times",char.class,new Dimension[]{timeD, dateStrLengthD}));
            
            //add Protovariable lainnya
            for (String namaProto : Line) 
            {
                listOfNamaProto.add(namaProto);
            }
            Line = reader.readNext(); //Mengambil nextLine (daftar satuan dari variabel dalam 1 baris csv)
            for(int i = 0; i < Line.length; i++)
            {
                String satuan = Line[i];
                listOfProtoVariable.add(new ProtoVariable(  listOfNamaProto.get(i),
                                                            float.class,
                                                            new Dimension[]{timeD, emissionD, southNorthD, westEastD},
                                                            new Attribute[]{new Attribute("units", satuan)}
                                                            )
                                        );
            }
            
            /* Membuat schema */
            ProtoVariable [] arrayProtovariabel = listOfProtoVariable.toArray(new ProtoVariable[0]);
            Attribute [] arrayGlobalAttribute = lisOfGlobalAttribute.toArray(new Attribute[0]);
            schema = new Schema(arrayProtovariabel, arrayGlobalAttribute);
            
            // Hapus variable times dari listOfProtoVariable agar tidak dihitung saat iterasi terakhir
            listOfProtoVariable.remove(0);
                    
            /* Write to netcdf file */
            NetcdfFile nc = new NetcdfFile(NamaFileNc, true, true, schema);
            arrayOfVariable = new Variable[listOfProtoVariable.size()]; 
            
            //Insert data Times
            Variable timesVar = nc.get("Times");
            for(int i = 0; i < dateStrLengthD.getLength(); i++)
            {
                timesVar.setChar(new int[]{0,i}, startDate.charAt(i));
            }
            
            //Insert data lainnya
            for (int timeIndex = 0; timeIndex < 1; timeIndex++)
            {
                for(int emisIndex = 0; emisIndex < emissionD.getLength(); emisIndex++)
                {
                    for (int southNorthIndex = 0; southNorthIndex < southNorthD.getLength(); southNorthIndex++)
                    {
                        for(int westEastIndex = 0; westEastIndex < westEastD.getLength(); westEastIndex++)
                        {
                            //Meng-assign nilai masing - masing variabel dari 1 baris
                            Line = reader.readNext();
                            
                            for(int variableIndex = 0; variableIndex < arrayOfVariable.length; variableIndex++)
                            {
                                arrayOfVariable[variableIndex] = nc.get((listOfNamaProto.get(variableIndex)));
                                
                                if(arrayOfVariable[variableIndex] != null)
                                {
                                    //System.out.println(timeIndex + " " + southNorthIndex + " " + westEastIndex + " " + variableIndex);
                                    arrayOfVariable[variableIndex].setFloat(new int[]{timeIndex, emisIndex, southNorthIndex, westEastIndex}, 
                                                                            Float.parseFloat(Line[variableIndex]));
                                }
                            }
                            
                        }
                    }
                }
            }
            nc.close();
            
        } catch (FileNotFoundException ex) {
            System.out.println("File not found");
        } catch (IOException ex) {
            System.out.println("IO eror in reader.readNext()");
        }
        
    }
}
