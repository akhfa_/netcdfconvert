/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package netcdfconvert;

import com.opencsv.CSVReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import ucar.netcdf.Attribute;
import ucar.netcdf.Dimension;
import ucar.netcdf.NetcdfFile;
import ucar.netcdf.ProtoVariable;
import ucar.netcdf.Schema;
import ucar.netcdf.UnlimitedDimension;
import ucar.netcdf.Variable;

/**
 *
 * @author akhfa
 */
public class createNCFinal {
    
    public static void main(String [] args)
    {
        final String NamaFileCsv = "file.csv";
        CSVReader reader;
        String NamaFileNc = null;
        int Axis;
        int Ordinat;
        String [] Line;
        List<Attribute> lisOfGlobalAttribute = new ArrayList<>();
        List<ProtoVariable> listOfProtoVariable = new ArrayList<>();
            List<String> listOfNamaProto = new ArrayList<>();
        
        List<Variable> listOfVariable = new ArrayList<>();
        Variable [] arrayOfVariable;
        
        Dimension timeD;
        Dimension westEastD;
        Dimension southNorthD;
        Dimension dateStrLenth;
        Dimension emissionD;
        
        Schema schema = null;
        
        
        try {
            reader = new CSVReader(new FileReader(NamaFileCsv));
            Line = reader.readNext();   //Ambil nama file
            NamaFileNc = Line[1];
            Line = reader.readNext();   //Ambil X
            Axis = Integer.parseInt(Line[1]);
            Line = reader.readNext();   //Ambil Y
            Ordinat = Integer.parseInt(Line[1]);
            
            /* Bikin dimensi. Sementara statis dulu */
            Line = reader.readNext();   //Ambil dimensi. Ini cuma baris dummy
            timeD = new UnlimitedDimension("time");
            westEastD = new Dimension("west_east", Axis);
            southNorthD = new Dimension("south_north", Ordinat);
            dateStrLenth = new Dimension(("DateStrLen"), 19);
            emissionD = new Dimension("emissions_zdim", 1);
            
            /* Baca global attribut */
            Line = reader.readNext();   //Ambil Attribute. Ini cuma baris dummy
            while(!(Line = reader.readNext())[0].equals("end"))
            {
                System.out.println("Line[0] = " + Line[0]);
                try
                {
                    lisOfGlobalAttribute.add(new Attribute(Line[0], Integer.parseInt(Line[1])));
                }
                catch(NumberFormatException e)
                {
                    lisOfGlobalAttribute.add(new Attribute(Line[0], Line[1]));
                }
            }   System.out.println("Keluar dari while attribut");
            
            /* Mulai ambil attribut. Dimulai dari baris yang isinya "data" */
            Line = reader.readNext(); //Ambil data. Ini cuma baris dummy
            Line = reader.readNext(); //Mengambil nextLine (daftar nama variabel dalam 1 baris csv)
            for (String namaProto : Line) 
            {
                listOfNamaProto.add(namaProto);
            }
            Line = reader.readNext(); //Mengambil nextLine (daftar satuan dari variabel dalam 1 baris csv)
            for(int i = 0; i < Line.length; i++)
            {
                String satuan = Line[i];
                listOfProtoVariable.add(new ProtoVariable(  listOfNamaProto.get(i),
                                                            float.class,
                                                            new Dimension[]{timeD, emissionD, southNorthD, westEastD},
                                                            new Attribute[]{new Attribute("units", satuan)}
                                                            )
                                        );
            }

            /* Membuat schema */
            ProtoVariable [] arrayProtovariabel = listOfProtoVariable.toArray(new ProtoVariable[0]);
            Attribute [] arrayGlobalAttribute = lisOfGlobalAttribute.toArray(new Attribute[0]);

            schema = new Schema(arrayProtovariabel, arrayGlobalAttribute);
            
            /* Write to netcdf file */
            NetcdfFile nc = new NetcdfFile(NamaFileNc, true, true, schema);
            arrayOfVariable = new Variable[listOfProtoVariable.size()]; 
                    
            for (int timeIndex = 0; timeIndex < 1; timeIndex++)
            {
                for(int emisIndex = 0; emisIndex < emissionD.getLength(); emisIndex++)
                {
                    for (int southNorthIndex = 0; southNorthIndex < southNorthD.getLength(); southNorthIndex++)
                    {
                        for(int westEastIndex = 0; westEastIndex < westEastD.getLength(); westEastIndex++)
                        {
                            //Meng-assign nilai masing - masing variabel dari 1 baris
                            Line = reader.readNext();
                            
                            for(int variableIndex = 0; variableIndex < arrayOfVariable.length; variableIndex++)
                            {
                                arrayOfVariable[variableIndex] = nc.get((listOfNamaProto.get(variableIndex)));
                                
                                if(arrayOfVariable[variableIndex] != null)
                                {
                                    arrayOfVariable[variableIndex].setFloat(new int[]{timeIndex, emisIndex, southNorthIndex, westEastIndex}, 
                                                                            Float.parseFloat(Line[variableIndex]));
                                }
                            }
                            
                        }
                    }
                }
            }
            nc.close();
            
        } catch (FileNotFoundException ex) {
            System.out.println("File not found");
        } catch (IOException ex) {
            System.out.println("IO eror in reader.readNext()");
        }
        
    }
}
